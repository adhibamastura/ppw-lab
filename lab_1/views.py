from django.shortcuts import render, redirect
from datetime import datetime, date
from .models import Schedule
# Enter your name here
mhs_name = 'Adhiba Mastura' # TODO Implement this
curr_year = int(datetime.now().strftime("%Y"))
birth_date = date(1998, 12, 9) #TODO Implement this, format (Year, Month, Date)
npm = 1706074764 # TODO Implement this
# Create your views here.
def index(request):
    response = {'name': mhs_name, 'age': calculate_age(birth_date.year), 'npm': npm}
    return render(request, 'index_lab1.html', response)

def home(request):
    response = {}
    return render(request, 'home.html', response)

def about(request):
    response = {}
    return render(request, 'about.html', response)

def critics(request):
    response = {}
    return render(request, 'critics.html', response)

def form(request):
    response = {}
    return render(request, 'form.html', response)

def schedule(request):
    response = {}
    if request.method=='POST' and 'add' in request.POST:
        date = datetime.strptime(request.POST['date'],'%Y-%m-%dT%H:%M')
        activity = request.POST['activity']
        place = request.POST['place']
        category = request.POST['category']
        Schedule.objects.create(date = date, activity = activity, place = place, category = category)
        return redirect('/lab-1/schedule')

    if request.method=='POST' and 'deleteall' in request.POST:
        Schedule.objects.all().delete()
        return redirect('/lab-1/schedule')

    response['schedule_dict'] = Schedule.objects.all().values()

    return render(request, 'schedule.html', response)

def calculate_age(birth_year):
    return curr_year - birth_year if birth_year <= curr_year else 0
