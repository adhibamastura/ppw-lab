# Generated by Django 2.1.1 on 2018-10-03 13:31

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('lab_1', '0002_auto_20181003_1909'),
    ]

    operations = [
        migrations.CreateModel(
            name='Schedule',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('date', models.DateTimeField()),
                ('activity', models.TextField(max_length=300)),
                ('place', models.TextField(max_length=300, null=True)),
                ('category', models.TextField(max_length=300, null=True)),
            ],
        ),
        migrations.DeleteModel(
            name='JadwalPribadi',
        ),
    ]
