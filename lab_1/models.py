from django.db import models

# Create your models here.
class Schedule (models.Model):
        date = models.DateTimeField()
        activity = models.TextField(max_length = 300)
        place = models.TextField(max_length = 300, null=True)
        category = models.TextField	(max_length = 300, null=True)
