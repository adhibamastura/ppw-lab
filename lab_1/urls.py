from django.urls import re_path
from .views import *
#url for app
app_name = "lab_1"
urlpatterns = [
    re_path(r'^$', index, name='index'),
    re_path(r'^home$', home, name='home'),
    re_path(r'^about$', about, name='about'),
    re_path(r'^critics$', critics, name='critics'),
    re_path(r'^form$', form, name='form'),
    re_path(r'^schedule$', schedule, name='schedule'),
]	
