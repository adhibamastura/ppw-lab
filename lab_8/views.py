from django.shortcuts import render, redirect

def index(request):
    response = {}
    return render(request, 'index.html', response)