from django.test import TestCase, LiveServerTestCase
from django.test import Client
from django.urls import resolve
from django.http import HttpRequest
from .views import index
from selenium import webdriver
from selenium.webdriver.chrome.options import Options
from selenium.webdriver.common.keys import Keys
import selenium.webdriver.chrome.service as service
import time


# Create your tests here.

class Story6UnitTest(TestCase):
    def test_url_is_exist(self):
        response = Client().get('/lab-8/')
        self.assertEqual(response.status_code,200)

    def test_using_index_template(self):
        response = Client().get('/lab-8/')
        self.assertTemplateUsed(response,'index.html')

    def test_landing_page_is_completed(self):
        request = HttpRequest()
        response = index(request)
        html_response = response.content.decode('utf8')
        self.assertIn('Ubah Tema', html_response)

class Story6FunctionalTest(LiveServerTestCase):
    def setUp(self):
        chrome_options = Options()
        chrome_options.add_argument('--dns-prefetch-disable')
        chrome_options.add_argument('--no-sandbox')
        chrome_options.add_argument('--headless')
        chrome_options.add_argument('disable-gpu')
        self.selenium  = webdriver.Chrome('./chromedriver', chrome_options=chrome_options)
        self.selenium.implicitly_wait(25) 
        super(Story6FunctionalTest,self).setUp()

    def test_title_position(self):
        selenium = self.selenium
        selenium.get(self.live_server_url)
        title = selenium.find_element_by_tag_name('h1')
        title_loc = title.location
        self.assertEqual(title_loc, {'x': 8, 'y': 21})

    def test_tag_h1(self):
        selenium = self.selenium
        selenium.get(self.live_server_url)
        titleh1_align = selenium.find_element_by_tag_name('h1').value_of_css_property('text-align')
        titleh1_fontSize = selenium.find_element_by_tag_name('h1').value_of_css_property('font-size')
        self.assertEqual(titleh1_align, 'center')
        self.assertEqual(titleh1_fontSize, '32px')

    # def test_change_theme(self):
    #     selenium = self.selenium
    #     selenium.get(self.live_server_url)
    #     button = selenium.find_element_by_tag_name('button').click()
    #     titleh1_color = selenium.find_element_by_tag_name('h1').value_of_css_property('color')
    #     self.assertEqual(titleh1_color, 'snow')

    def tearDown(self):
        self.selenium.quit()
        super(Story6FunctionalTest, self).tearDown()

